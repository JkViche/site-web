<?php
    try{
        $bdd = new PDO('mysql:host=localhost;dbname=bdd_2_9;charset=utf8', 'grp_2_9', 'Ohvaey0loo');
    //verification
    if(!empty($_POST['identifiant']) AND !empty($_POST['adressemail']) AND !empty($_POST['motDePasse'])){
    //recupération de l'identifiant et de l'adresse mail entrés
    $_SESSION['id'] = $_POST['identifiant'];
    $_SESSION['adresse'] = $_POST['adressemail'];
    //Verification de l'unicite de l'identifiant
    $req = $bdd->prepare('SELECT identifiant FROM inscrit WHERE  identifiant = ?');
    $req->execute(array($_POST['identifiant']));
    $resultat = $req->fetch();
    if(strcmp($resultat['identifiant'], $_POST['identifiant'])==0){
        header('Location: https://moduleweb.esigelec.fr/grp_2_9/PageInscription.php?test_identifiant=0');
        exit();
    }
    //Verification de l'unicite de l'adresse mail
    $req = $bdd->prepare('SELECT email FROM inscrit WHERE  email = ?');
    $req->execute(array($_POST['adressemail']));
    $resultat = $req->fetch();
    if(strcmp($resultat['email'], $_POST['adressemail'])==0){
        header('Location: https://moduleweb.esigelec.fr/grp_2_9/PageInscription.php?test_mail=0');
        exit();
    }
    //hachage du mot de passe 
    $pass_hache = password_hash($_POST['motDePasse'], PASSWORD_DEFAULT);
    if(strcmp($_POST['motDePasse'],$_POST['confirmPassword'])==0){
        // Insertion
        $req = $bdd->prepare('INSERT INTO inscrit(identifiant, email, mdp, vote) VALUES(:identifiant, :email, :mdp, 0)');
        $req->execute(array(
            'identifiant' => $_POST['identifiant'],
            'email' => $_POST['adressemail'],
            'mdp' => $pass_hache));
            //message de confirmation
            header('Location: https://moduleweb.esigelec.fr/grp_2_9/PageConnexion.php?inscription=1');
         exit();
    }
    else{
        header('Location: https://moduleweb.esigelec.fr/grp_2_9/PageInscription.php?test_mdp=0&identifiant='.$_POST['identifiant'].'&email='.$_POST['adressemail']);
        exit();
    }
}
elseif(empty($_POST['identifiant']) OR empty($_POST['adressemail']) OR empty($_POST['motDepasse'])){
    header('Location: https://moduleweb.esigelec.fr/grp_2_9/PageInscription.php?champ_vide=1&identifiant='.$_POST['identifiant'].'&email='.$_POST['adressemail']);
    exit();
}
    }
    catch(Exception $e){
        die('Erreur : ' . $e->getMessage());
    }
?>