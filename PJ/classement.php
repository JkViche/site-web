<!doctype html>
<html lang="fr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="pageA.css" media="all"/>
    <link rel="stylesheet" href="pageVoter.css" media="all"/>
    <script type="text/javascript" src="Site.js"></script>
		<title>Classement</title>
	</head>
	<body>
		<div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div id="principale">
                    <?php
							        include 'menu.inc.php'; 
					        	?>
                        <div class="row justify-content-md-center">
                            <div class="col-10 col-sm-8 col-md-8 col-lg-6 col-xl-5">
                                <article>
                                </form>
											<!-- Trigger/Open The Modal>
									<button id="myBtn">Open Modal</button-->
									<!-- The Modal -->
									<div id="fermer" class="modal">
									<!-- Modal content -->
									<form action="classer.php" method="post" class="modal-content" id="form_demarrage">
										<div class="modal-header">
											<h2>Veuillez choisir une élection</h2>
											<span class="close1">&times;</span>
										</div>
										<div class="modal-body">
                        <?php
                          $compteur=0;
                          include("cnx.php");     
                          $req = "SELECT nom " . "FROM election WHERE statut=0";
                          $ret = mysqli_query ($cnx, $req) or die (mysqli_error ($cnx));
                          while( $col = mysqli_fetch_row ($ret))
                          { ?>
                              <label class="btn btn-secondary">
                                <input type="radio" name="options" <?php echo 'value="'.$col[0].'"'?>> <?php echo $col[0]?><br/>
                              </label>
                          <?php
                                      }
                          ?>
										</div>
											<div class="modal-footer">
											  <button type="submit" class="btn btn-light">Valider</button>
											</div>	
									</form>		
                  </div>								
									<script type="text/javascript">
										// Get the modal 
										var modal2 = document.getElementById("fermer");
										// Get the <span> element that closes the modal
										var span2 = document.getElementsByClassName("close1")[0];
										modal2.style.display = "block";
										// When the user clicks on <span> (x), close the modal
										span2.onclick = function() {
										modal2.style.display = "none";
										}
										// When the user clicks anywhere outside of the modal, close it
										window.onclick = function(event) {
										if (event.target == modal2) {
											modal2.style.display = "none";
										}
                    } 
                  </script>
									<?php
        include ("cnx.php");
       // $req  = "SELECT img_id,img_nom,img_vote,img_rang " . 
               //"FROM image GROUP BY img_id ORDER BY img_vote DESC, img_nom ASC";
		
//$ret = mysqli_query($cnx, $req) or die('Erreur SQL !<br />'.$ret.'<br />'.mysqli_error($cnx));
 
 $req_pre = mysqli_prepare($cnx,'SELECT img_id, img_nom, img_vote, img_rang FROM image WHERE id_election=? GROUP BY img_id ORDER BY img_vote DESC, img_nom ASC');
 mysqli_stmt_bind_param($req_pre,"i",$_GET['id_election']);
 mysqli_stmt_execute($req_pre);
 mysqli_stmt_bind_result($req_pre,$col1,$col2,$col3,$col4);
//echo $col1;
//echo 'bonjour';
if(mysqli_stmt_fetch($req_pre)) {
  $rang = 1;
  $votes = 1;
$incr = 1;
  echo '<table width="500px">'."\n";
  echo '<tr height="25" id="legend">';
  echo '<td align="center"><b>Clt</b></td>';
    echo '<td align="center"><b>ID</b></td>';
  echo '<td align="center"><b>Posters</b></td>';
  echo '<td align="center"><b>Votes</b></td>';
  echo '</tr>'."\n";

   do {
    $rang = $votes == $col3 ? $rang : $incr;
  $incr++;
     $votes = $col3;
    echo '<tr>';
  echo '<td align="center">'.$rang.'</td>';
		   echo '<td align="center">'.$col1.'</td>';
      echo '<td align="center">'.$col2.'</td>';
  	  echo '<td align="center">'.$col3.'</td>';

    } while(mysqli_stmt_fetch($req_pre));

    echo '</table>'."\n";
}
else
{
  echo '<div align="center"><h2>Aucun classement à afficher</h2></div>';
	}
  //mysqli_free_result($ret);	
?>
<?php
		if(isset($_GET['id_election'])){
			echo '<script>
					var modal2 = document.getElementById("fermer");
					modal2.style.display = "none";
			</script>';
		}
?>
                                </article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
