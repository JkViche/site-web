<?php
	session_start();// démarrage de la session
?>
<!doctype html>
<html lang="fr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script type="text/javascript" src="Site.js"></script>
		<link rel="stylesheet" href="pageA.css" media="all"/>
		<link rel="stylesheet" href="pageVoter.css" media="all"/>
		<title>Responsable page</title>
	</head>
	<body>
		<div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div id="principale">
						<?php
							include 'menu.inc.php'; 
						?>
                        <div class="row justify-content-md-center">
                            <div class="col-12 col-sm-12 col-md-10 col-lg-8 col-xl-7">
                                <article id="responsable">
									<h1>Quelle action souhaitez-vous réaliser ?</h1>
									<span id="form-control">
                                     <?php
                                        if(isset($_GET['election_en_cours']) AND $_GET['election_en_cours']==1){
                                            echo 'Une élection est déjà en cours. Vous devez l\'arrêter afin de pouvoir en créer une autre';
                                        }
										//echo $_SESSION['id_election'];
                                    ?>
                                 </span>									
									<nav class="nav nav-pills flex-column flex-sm-row" id="choix">
									<a class="flex-sm-fill text-sm-center nav-link"  id="myBtn1"><br/><img src="images/icones/power-button.png"/><br/>Démarrer une élection</a>
									<a class="flex-sm-fill text-sm-center nav-link"  id="myBtn2"><br/><img src="images/icones/stop.png"/><br/>Arrêter l'élection en cours</a>
									<a class="flex-sm-fill text-sm-center nav-link" href="pageAjoutPoster.php" id="third"><br/><img src="images/icones/plus.png"/><br/>Ajouter un poster</a>
									<a class="flex-sm-fill text-sm-center nav-link" href="pageAffichage_poster.php" id="fourth"><br/><img src="images/icones/election.png"/><br/>Voter pour un poster</a>
									</nav>
									</article> 
									<!-- Trigger/Open The Modal>
									<!-- The Modal -->
									<div id="demarrer" class="modal">
									<!-- Modal content -->
									<form action="demarrer.php" method="post" class="modal-content" id="form_demarrage">
										<div class="modal-header">
											<h2>Démarrage d'une nouvelle élection</h2>
											<span class="close1">&times;</span>
										</div>
										<div class="modal-body">
												<label for="nom_election">Nom Election</label>
												<input type="text" class="form-control" name="nom_election" placeholder="Entrer un nom pour l'election"/>
												<?php 
													if(isset($_GET['nom_pris']) AND $_GET['nom_pris']==1){
														echo '<span>Ce nom est déjà attribué à une autre élection</span>';
													}
												?>
												<!--?php if(isset($_GET['demarrage']) AND $_GET['demarrage']==0)?-->
												</div>
											<div class="modal-footer">
											<button type="submit" class="btn btn-light">Valider</button>
											</div>	
									</form>		
									</div>
									<!-- Trigger/Open The Modal>
									<!-- The Modal -->
									<div id="fermer" class="modal">
									<!-- Modal content -->
									<form action="fermer.php" method="post" class="modal-content" id="form_fermeture">
										<div class="modal-header">
											<h2>Confirmation de fermeture</h2>
											<span class="close2">&times;</span>
										</div>
										<div class="modal-body">
												<label for="nom_election">Voulez-vous vraiment arrêter l'élection en cours?</label>												
												</div>
											<div class="modal-footer">
											<button type="submit" class="btn btn-light">Oui</button>
											</div>	
									</form>	
									</div>							
									<script type="text/javascript">
									//demarrer
									//function modal1(){
									// Get the modal 
									var modal = document.getElementById("demarrer");
									// Get the button that opens the modal
									var btn = document.getElementById("myBtn1");
									// Get the <span> element that closes the modal
									var span = document.getElementsByClassName("close1")[0];

									// When the user clicks on the button, open the modal
									btn.onclick = function() {
									modal.style.display = "block";
									}

									// When the user clicks on <span> (x), close the modal
									span.onclick = function() {
									modal.style.display = "none";
									}

									// When the user clicks anywhere outside of the modal, close it
									window.onclick = function(event) {
									if (event.target == modal) {
									modal.style.display = "none";
									}
									} 
									//var controlChamp= document.getElementById('id-control');
									//controlChamp.textContent = "Votre mot de passe doit avoir au moins 6 caractères";
									//return true;
									//}
									//fermer
										// Get the modal 
										var modal2 = document.getElementById("fermer");

										// Get the button that opens the modal
										var btn2 = document.getElementById("myBtn2");

										// Get the <span> element that closes the modal
										var span2 = document.getElementsByClassName("close2")[0];

										// When the user clicks on the button, open the modal
										btn2.onclick = function() {
										modal2.style.display = "block";
										}

										// When the user clicks on <span> (x), close the modal
										span2.onclick = function() {
										modal2.style.display = "none";
										}

										// When the user clicks anywhere outside of the modal, close it
										window.onclick = function(event) {
										if (event.target == modal2) {
											modal2.style.display = "none";
										}
										} 
									</script>
									<?php 
										if(isset($_GET['nom_pris']) AND $_GET['nom_pris']==1){
											echo '<script>
											var modal = document.getElementById("demarrer");
											modal.style.display = "block";
											</script>';
										}
									?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>