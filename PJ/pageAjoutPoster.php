<?php
	session_start();// démarrage de la session
?>
<!doctype html>
<html lang="fr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="pageA.css" media="all"/>
		<link rel="stylesheet" href="pageVoter.css" media="all"/>
		<title>Page d'ajout de posters</title>
	</head>
	<body>
		<div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div id="principale">
                        <?php
							include 'menu.inc.php'; 
				
						?>
                        <div class="row justify-content-md-center">
                            <div class="col-10 col-sm-8 col-md-8 col-lg-6 col-xl-5">
							<?php ?>
                                <article>

      <h3>Envoi d'une image</h3>
      <form enctype="multipart/form-data" action="#" method="post">
         <input type="hidden" name="MAX_FILE_SIZE" value="500000000" />
         <input type="file" name="monfichier" size=30 />
 
		 
		
<label for="img_desc">Description</label>
<input type="text" name="img_desc" id="img_desc" value="" required>
<br>
        <input type="submit" value="Envoyer" />
      </form>
<?php
 include ("cnx.php");
         include ("transfert.php");
         if ( isset($_FILES['monfichier']) )
         {
             transfert();
         }
     ?>
	
	                               <?php
									// Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
									if (isset($_FILES['monfichier']) AND $_FILES['monfichier']['error'] == 0)
									{
									// Testons si le fichier n'est pas trop gros
										if ($_FILES['monfichier']['size'] <= 500000000)
										{
										// Testons si l'extension est autorisée
										$infosfichier = pathinfo($_FILES['monfichier']['name']);
										$extension_upload = $infosfichier['extension'];
										$extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
										if (in_array($extension_upload, $extensions_autorisees))
										{
										// On peut valider le fichier et le stocker définitivement
											move_uploaded_file($_FILES['monfichier']['tmp_name'], 'uploads/' . basename($_FILES['monfichier']['name']));
											echo "L'envoi a bien été effectué !";
											
										}
										}
									}

?>	


                                </article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
