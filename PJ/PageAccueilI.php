<?php
	session_start();// démarrage de la session
?>
<!doctype html>
<html lang="fr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="pageA.css" media="all"/>
		<title>Homepage</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="principale">
                        <nav class="nav nav-pills flex-column flex-sm-row">
                            <a class="flex-sm-fill text-sm-center nav-link" href="#" id="this">Home</a>
                            <a class="flex-sm-fill text-sm-center nav-link" href="pageAffichage_poster.php">Posters</a>
                            <a class="flex-sm-fill text-sm-center nav-link" href="classement.php">Classement</a>
                            <div class="flex-sm-fill text-sm-center nav-link" id="logo">Ping</div>
							<a class="flex-sm-fill text-sm-center nav-link active" href="PageInscription.php" id="inscription">
								<?php
									if(!empty($_SESSION['identifiant'])){
										echo $_SESSION['identifiant'];
									}
									else{
										echo "Inscription";
										}
								?>
							</a>
                            <a class="flex-sm-fill text-sm-center nav-link active" href="PageConnexion.php" id="connexion">
							<?php
									if(!empty($_SESSION['identifiant'])){
										echo "Déconnexion";
									}
									else{
										echo "Connexion";
										}
								?>
							</a>
                            </nav>
						<div class="row justify-content-md-center">
							<div class="col-12 col-sm-10 col-md-10 col-lg-8 col-xl-7">
								<article>
									<h1>N'ATTENDEZ PLUS POUR VOTER !</h1>
										<p>Bienvenue sur notre site web. Nous sommes très ravis de vous accueillir dans le cadre de ces élections ô 
										combien importantes pour le futur et la carrière de nos jeunes ingénieurs. 
										Ils ont travaillé toute l’année sans relâche et sont aujourd’hui impatients de vous présenter leurs œuvres</p>
								<article>
							</div>
						</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<aside>
						<img src="images/icons8-target-64.png" alt="Objectifs"/>
					</aside>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="middleUp">
                        <h1>Présentation de l'équipe</h1>
                        <div class="card-deck">
							<div class="card">
								<img src="images/students.jpg" class="card-img-top" alt="...">
								<div class="card-body">
								<h5 class="card-title">Card title</h5>
								<p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
								<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
								</div>
							</div>
							<div class="card">
								<img src="images/students.jpg" class="card-img-top" alt="...">
								<div class="card-body">
								<h5 class="card-title">Card title</h5>
								<p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
								<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
								</div>
							</div>
							<div class="card">
								<img src="images/students.jpg" class="card-img-top" alt="...">
								<div class="card-body">
								<h5 class="card-title">Card title</h5>
								<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
								<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="middleCenter"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="middleDown">
                        <h1>N'hésitez pas à passer l'information !</h1>
                       <p id="link_esigelec"> <a href="http://www.esigelec.fr/fr">Esigelec</a></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<footer>
                        <div id="trait"></div>
                    </footer>	
				</div>
			</div>
		</div>
	</body>
</html>