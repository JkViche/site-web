<?php
	session_start();// démarrage de la session
?>
<!doctype html>
<html lang="fr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script type="text/javascript" src="Site.js"></script>
        <link rel="stylesheet" href="pageA.css" media="all"/>
        <link rel="stylesheet" href="PageInscription.css" media="all"/>
		<title>Homepage</title>
	</head>
	<body>
		<div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div id="principale">
                    <?php
							include 'menu.inc.php'; 
						?>
                        <div class="row justify-content-md-center">
                            <div class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-6">
                                <article>
                                <h1>Création de compte</h1>
                                <span id="form-control">
                                     <?php
                                        if(isset($_GET['test_identifiant']) AND $_GET['test_identifiant']==0){
                                            echo 'Veuillez choisir un autre identifiant';
                                        }
                                        elseif(isset($_GET['test_mail']) AND $_GET['test_mail']==0){
                                            echo 'Cette adresse email est déjà utilisée par un autre compte';
                                        }
                                         elseif(isset($_GET['test_mdp']) AND $_GET['test_mdp']==0){
                                             echo 'Veuillez saisir le même mot de passe dans les deux champs';
                                         }
                                         else if(isset($_GET['champ_vide']) AND $_GET['champ_vide']==1){
                                             echo 'Veuillez remplir tous les champs';
                                        }
                                    ?>
                                 </span>
                                    <form action="inscription.php" method="post" onsubmit="return verifForm(this)">
                                            <div class="form-group">
                                                    <label for="identifiant">Identifiant</label>
                                                    <div class="input-group mb-2">
                                                      <div class="input-group-prepend">
                                                        <div class="input-group-text">@</div>
                                                      </div>
                                                      <input type="text" class="form-control" name="identifiant" placeholder="Entrer un Identifiant" 
                                                      <?php if(isset($_SESSION['id'])){echo 'value="'.$_SESSION['id'].'"';}?>" onblur="verifIdentifiant(this)">
                                                      <span id="id-control"></span>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                              <label for="adressemail">Adresse mail</label>
                                              <input type="email" class="form-control" name="adressemail" aria-describedby="emailHelp" placeholder="Entrer votre adresse mail" 
                                              value="<?php if(isset($_SESSION['adresse'])){echo $_SESSION['adresse'];}?>" onblur="verifMail(this)">
                                              <span id="mail-control"></span>
                                            </div> 
                                            <div class="form-group">
                                              <label for="mot de passe">Mot de passe</label>
                                              <input type="password" class="form-control" name="motDePasse" placeholder="Entrer un mot de passe" onblur="verifMdp(this)">
                                              <span id="mdp-control"></span>
                                            </div>
                                            <div class="form-group">
                                                    <label for="confirmpassword">Confirmation mot de passe</label>
                                                    <input type="password" class="form-control" name="confirmPassword" placeholder="Entrer de nouveau votre mot de passe" onblur=verifConfirmMdp(this)>
                                                    <span id="confirmMdp-control"></span> 
                                                </div>
                                            <button type="submit" class="btn btn-primary">Valider</button>
                                     </form>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</body>
</html>