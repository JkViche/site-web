function affichagePoster(modal,bouton,span){
									// Get the modal 
									var modal = document.getElementById(modal);
									// Get the button that opens the modal
									var btn = document.getElementById(bouton);
									// Get the <span> element that closes the modal
									var span = document.getElementsByClassName(span)[0];

									// When the user clicks on the button, open the modal
									btn.onclick = function() {
									modal.style.display = "block";
									}

									// When the user clicks on <span> (x), close the modal
									span.onclick = function() {
									modal.style.display = "none";
									}

									// When the user clicks anywhere outside of the modal, close it
									window.onclick = function(event) {
									if (event.target == modal) {
									modal.style.display = "none";
									}
									}
return true;									
}
//Affichage menu déroulant
/* Open when someone clicks on the span element */
function openNav() {
     document.getElementById("myNav").style.width = "100%";
   }
   
   /* Close when someone clicks on the "x" symbol inside the overlay */
   function closeNav() {
     document.getElementById("myNav").style.width = "0%";
   }
//Slideshow images
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
} 
//fonctions formulaires
function verifIdentifiant(champ)
{
    var controlChamp= document.getElementById('id-control');
   if(champ.value.length < 2)
   {
       controlChamp.textContent="Votre identifiant doit avoir au moins 2 caractères";
        return false;
   }
   else if( champ.value.length > 10)
   {
      controlChamp.textContent="Votre identifiant doit avoir au plus 10 caractères";
   }
   else 
   {
        controlChamp.textContent=" ";
        return true;
   }
}
function verifMail(champ)
{
    var controlChamp=document.getElementById('mail-control');
    var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
   if(!regex.test(champ.value))
   {
      controlChamp.textContent = "Adresse mail non valide";
      return false;
   }
   else
   {
        controlChamp.textContent=" ";
        return true;
   }
}
function verifMdp(champ)
{
    var controlChamp=document.getElementById('mdp-control');
   if(champ.value.length < 6)
   {
    controlChamp.textContent = "Votre mot de passe doit avoir au moins 6 caractères";
        return false;
   }
   else 
   {
         controlChamp.textContent=" ";
        return true;
   }
}
function verifConfirmMdp(champ)
{
    var controlChamp=document.getElementById('confirmMdp-control');
   if(champ.localeCompare(document.forms[0].motDePasse)!=0)
   {
    controlChamp.textContent = "Veuillez saisir le même mot de passe";
        return false;
   }
   else 
   {
        controlChamp.textContent=" ";
        return true;
   }
}
function verifForm(f)
{
    var controlForm= document.getElementById('form-control');
    var pseudoOk = verifIdentifiant(f.identifiant);
    var mailOk = verifMail(f.adressemail);
    var ageOk = verifMdp(f.motDePasse);
   
   if(pseudoOk && mailOk && ageOk){
        controlChamp.textContent=" ";
        return true;
   }
   else
   {
        controlForm.textContent= "Veuillez remplir correctement tous les champs";
        return false;
   }
}