<?php
	session_start();// démarrage de la session
?>
<!doctype html>
<html lang="fr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="pageA.css" media="all"/>
		<script type="text/javascript" src="Site.js"></script>
		<title>Homepage</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="principale">
						<?php
							include 'menu.inc.php'; 
				
						?>
						<div class="row justify-content-md-center">
							<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-7">
								<article>
									<h1>N'ATTENDEZ PLUS POUR VOTER !</h1>
										<p>Bienvenue sur notre site web. Nous sommes très ravis de vous accueillir dans le cadre de ces élections ô 
										combien importantes pour le futur et la carrière de nos jeunes ingénieurs. 
										Ils ont travaillé toute l’année sans relâche et sont aujourd’hui impatients de vous présenter leurs œuvres</p>
							
								
								</article>
							</div>
						</div>
				</div>
			</div>
		</div>
					<aside>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h2>CONTEXTE DU PROJET</h2>
						<p>
						Le Ping se déroule en 3ème année du cycle ingénieur, en semestre 9, par équipe de 6. Il permet de réaliser un projet dans des conditions
						quasi industrielles, en relation avec un commanditaire (en majorité une entreprise).
						Le projet souvent multidisciplinaire, regroupe des étudiants de différentes dominantes, comme de différentes voies d’accès (cursus classique, apprentissage, cursus formation continue Fontanet).
						Chaque équipe a pour mission de :
						</p>
						</div>
					</div>
						<div class="card-deck">
							<div class="card">
								<img src="images/analysis.png" class="card-img-top" alt="Image étude">
								<div class="card-body">
								<p class="card-text">Réaliser une étude de faisabilité technico économique</p>
								</div>
							</div>
							<div class="card">
								<img src="images/notebook.png" class="card-img-top" alt="Image cahier">
								<div class="card-body">
								<p class="card-text">Elaborer le cahier des charges</p>
								</div>
							</div>
							<div class="card">
								<img src="images/graph.png" class="card-img-top" alt="Image graphe">
								<div class="card-body">
								<p class="card-text">Exposer publiquement le résultat de ses travaux</p>
								</div>
							</div>
						</div>
					</aside>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="middleUp">
                        <h2>PRESENTATION DU PING</h2>
                        <div class="card-deck">
							<div class="card">
								<img src="images/students.jpg" class="card-img-top" alt="...">
								<div class="card-body">
								<h5 class="card-title">Objectifs</h5>
								<p class="card-text">Ce projet doit permettre aux élèves ingénieurs de développer des compétences techniques en rapport avec le projet et leurs compétences, d'apprendre à s'organiser et à gérer le temps</div>
							</div>
							<div class="card">
								<img src="images/students.jpg" class="card-img-top" alt="...">
								<div class="card-body">
								<h5 class="card-title">Etapes</h5>
								<p class="card-text">Chaque équipe réalise une étude de faisabilité technico économique validée par la structure donneuse d'ordre  puis élabore le cahier des charges. Ils réalisent ensuite le projet et un poster ping associé </p>
								</div>
							</div>
							<div class="card">
								<img src="images/students.jpg" class="card-img-top" alt="...">
								<div class="card-body">
								<h5 class="card-title">Ressources</h5>
								<p class="card-text"> Le projet souvent multidisciplinaire, regroupe des étudiants de différents dominantes , comme de différentes voies d'accès (cursus classique, apprentissage, cursus formation continue Fontanet</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div id="middleCenter">
					<div class="row">
						<!--div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-3"></div-->
						<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-7" id="partenaires">
							<h2>UN PROJET SOUTENU PAR LES LEADERS DU MONDE PROFESSIONNEL</h2>
						<img src="images/partenaires/1.png" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/2.jpg" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/3.png" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/4.png" class="img-fluid" alt="Responsive image">
						
						<img src="images/partenaires/5.jpg" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/6.png" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/7.png" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/8.jpg" class="img-fluid" alt="Responsive image">
					
						<img src="images/partenaires/9.png" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/10.jpg" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/11.png" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/12.jpg" class="img-fluid" alt="Responsive image">
						
						<img src="images/partenaires/13.png" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/14.jpg" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/15.jpg" class="img-fluid" alt="Responsive image">
						<img src="images/partenaires/16.jpg" class="img-fluid" alt="Responsive image">
						
						</div>
					</div>
				</div>
				</div>
				</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="middleDown">
                        <h1>N'hésitez pas à passer l'information !</h1>
					  <p><a href="http://www.facebook.com/sharer.php?u=http://PageAccueil.php&t=Inscrits-toi_et_vote_pour_ton_poster_PING_préféré" target="_blank" id="link_partage">Partager</a></p>
					  <p>Visiter le Site Officiel de <a href="http://www.esigelec.fr/fr" target="_blank" id="link_esigelec">l'Esigelec</a></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<footer>
						<div class="card-deck">
							<div class="card">
								<div class="card-body" id="footer1">
								<p class="card-text">
									Facebook &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Likez notre page<br/><br/>
									Twitter &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Suivez nous <br/><br/>
									Instagram &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Abonnez-vous <br/><br/><br/><br/><br/><br/><br/>
									<strong>Made by</strong> Djekeye VICHE & MAMA Yasmine</p>
								</div>
							</div>
							<div class="card">
								<div class="card-body" id="footer2">
									<p class="card-text">
										(+33) 02 32 91 58 58<br/><br/>
										esigelec@esigelec.fr<br/><br/>
										Avenue Galilée<br/><br/>
										76801 Saint-Etienne-du-Rouvray Cedex<br/><br/>
										France
									</p>
								</div>
							</div>
						</div>
                    </footer>	
				</div>
			</div>
		</div>
	</body>
</html>