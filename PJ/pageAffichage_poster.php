<?php
	session_start();// démarrage de la session
?>
<!DOCTYPE html>
<html lang=fr>

<head>
    <meta charset="utf-8">
    <title>Page posters</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="pageA.css" media="all"/>
		<link rel="stylesheet" href="pageAffichage_poster.css" media="all"/>
		<script type="text/javascript" src="Site.js"></script>
</head>

<body>
 <div class="container-fluid">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<div id="principale">
						<?php
							include 'menu.inc.php'; 
				
						?>
            <!--div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="images/project-planning-header.png" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/ProjectManagement.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/cool.jpg" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div-->

        <div class="row">
            </br></br>
        </div>
  
        <div class="row">
        <?php
                        
				$bdd = new PDO('mysql:host=localhost;dbname=bdd_2_9;charset=utf8', 'grp_2_9', 'Ohvaey0loo');
                //  Récupération de l'utilisateur et de son pass hashé
                $req = $bdd->prepare('SELECT id FROM election WHERE  statut = ?');
                $req->execute(array(1));
                $resultat = $req->fetch();	
				include("cnx.php");
			   $req_pre = mysqli_prepare($cnx,'SELECT img_id, img_nom, img_lien, img_vote, img_desc  FROM image WHERE id_election=?');
			   mysqli_stmt_bind_param($req_pre,"i",$resultat['id']);
			   mysqli_stmt_execute($req_pre);
			   mysqli_stmt_bind_result($req_pre,$col1,$col2,$col3,$col4,$col5);
       
	   $compteur=0;
        while(mysqli_stmt_fetch($req_pre))
                        { ?>

                        <div class="col-md-4">
                                    <div class="card mb-4 shadow-sm">
                                        <img class="bd-placeholder-img card-img-top" width="100%" height="225"
                                        src="<?php echo $col3;?>" preserveAspectRatio="xMidYMid slice" focusable="false"
                                    role="img" aria-label="Placeholder: Thumbnail" alt="poster"/>
                                    
                                <div class="card-body">
                                    <p class="card-text"><?php echo $col5;?></p>
                                    <div class="d-flex justify-content-between align-items-center">
									<form method="post" action="voter.php?img_id=<?php echo $col1?>">
                                        <div class="btn-group">
												
										    
											<button type="submit" name="Voter" value="<?php echo $_SESSION['vote'] ?>" 
											<?php if(isset($_GET['vote']) AND$_GET['vote']==1){echo 'disabled="disabled"';}?>>Voter</button>
                                        </div>
									</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        			<!-- Trigger/Open The Modal>
									<!-- The Modal -->
									<div id="poster<?php echo $compteur?>" class="modal">
									<!-- Modal content -->
										<div class="modal-header">
											<h2>NOM</h2>
											<span class="close<?php echo $compteur?>">&times;</span>
										</div>
										<div class="modal-body">
											<img src="<?php echo $col3 ?>" class="img-fluid" alt="Responsive image">  <?php echo $col5;?>
										</div>
										<div class="modal-footer">
				
										</div>		
									</div>
									<script>
										// Get the modal 
									var modal = document.getElementById("poster<?php echo $compteur?>");
									// Get the button that opens the modal
									var btn = document.getElementById("myBtn<?php echo $compteur?>");
									// Get the <span> element that closes the modal
									var span = document.getElementsByClassName("close<?php echo $compteur?>")[0];

									// When the user clicks on the button, open the modal
									btn.onclick = function() {
									modal.style.display = "block";
									}

									// When the user clicks on <span> (x), close the modal
									span.onclick = function() {
									modal.style.display = "none";
									}

									// When the user clicks anywhere outside of the modal, close it
									window.onclick = function(event) {
									if (event.target == modal) {
									modal.style.display = "none";
									}
									}
									</script>
            <?php
                        }
            ?>
            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
            </script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
            </script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
            </script>
            </div>
        </div>
    </div>
	</div>
	</div>
</body>

</html>