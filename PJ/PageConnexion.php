<!doctype html>
<html lang="fr">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script type="text/javascript" src="Site.js"></script>
        <link rel="stylesheet" href="pageA.css" media="all"/>
        <link rel="stylesheet" href="PageConnexion.css" media="all"/>
		<title>Connexion page</title>
    </head>
	<body>
		<div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div id="principale">
                    <?php
							include 'menu.inc.php'; 
						?>
                        <div class="row justify-content-md-center">
                            <div class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-5">
                                <article>
                                    <h1>Mon compte</h1>
                                    <?php 
                                        if(isset($_GET['inscription']) AND $_GET['inscription']==1){
                                            echo '<p id="inscrit">Inscription réussie!!!<br/> Vous pouvez utiliser vos identifiants pour vous connecter</p>';
                                        }
                                        else if(isset($_GET['connexion']) AND $_GET['connexion']==0){
                                            echo '<p id="wrong"> Mauvais identifiant ou mot de passe</p>';
                                        }
                                    ?>
                                    <form action="connexion.php" method="post"><div class="form-group">
                                            <label for="identifiant">Identifiant</label>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
														<div class="input-group-text">@</div>
													</div>
													<input type="text" class="form-control" name="identifiant" placeholder="Entrer un Identifiant">
												</div>
									    </div>
                                         <div class="form-group">
											<label for="mot de passe">Mot de passe</label>
											<input type="password" class="form-control" name="motDePasse"  placeholder="Entrer un mot de passe">
										</div>
                                        <button type="submit" class="btn btn-primary">Valider</button>
                                     </form>
                                </article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>